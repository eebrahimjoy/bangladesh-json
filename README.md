# Bangladesh JSON 
# Division Json || District JSON || Upazila JSON || Postcode JSON 

### Divisions
* Division name (English)
* Division name (Bangla)
* Division Latitude and Longitude

### Districts
* District mapped with Divisions
* District name (English)
* District name (Bangla)
* District Latitude and Longitude

### Upazilas
* Upazila mapped with Districts
* Upazila name (Bangla)
* Upazila name (English)
* Upazila Latitude and Longitude

### Post Office and Post Code
* Post Office
* Post Code
